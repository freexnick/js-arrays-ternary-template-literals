const result = [];
//start of subjects
result["subjects"] = ["javascript", "python", "react", "java"];
result["subjects"]["frontend"] = ["javascript", "react"];
result["subjects"]["credits"] = [];
result["subjects"]["credits"]["javascript"] = 4;
result["subjects"]["credits"]["react"] = 7;
result["subjects"]["credits"]["python"] = 6;
result["subjects"]["credits"]["java"] = 3;
result["subjects"]["credits"]["summary"] =
  result["subjects"]["credits"]["javascript"] +
  result["subjects"]["credits"]["react"] +
  result["subjects"]["credits"]["python"] +
  result["subjects"]["credits"]["java"];
//end of subjects
//start of student list
result["students"] = [];
result["students"]["list"] = ["jR", "kD", "vG", "dS"];
//jr info
result["students"]["jR"] = [];
result["students"]["jR"]["name"] = "Jean";
result["students"]["jR"]["lastName"] = "Reno";
result["students"]["jR"]["age"] = "26";
//jr grades
result["students"]["jR"]["grades"] = [];
result["students"]["jR"]["grades"]["javascript"] = 62;
result["students"]["jR"]["grades"]["react"] = 57;
result["students"]["jR"]["grades"]["python"] = 88;
result["students"]["jR"]["grades"]["java"] = 90;
result["students"]["jR"]["grades"]["summary"] =
  result["students"]["jR"]["grades"]["javascript"] +
  result["students"]["jR"]["grades"]["react"] +
  result["students"]["jR"]["grades"]["python"] +
  result["students"]["jR"]["grades"]["java"];
result["students"]["jR"]["grades"]["average"] =
  result["students"]["jR"]["grades"]["summary"] / result["subjects"].length;
result["students"]["jR"]["grades"]["gp"] = [];
//calculate gp/gpa
if (
  result["students"]["jR"]["grades"]["javascript"] >= 51 &&
  result["students"]["jR"]["grades"]["javascript"] <= 61
) {
  result["students"]["jR"]["grades"]["gp"]["javascript"] = 0.5;
} else if (
  result["students"]["jR"]["grades"]["javascript"] >= 61 &&
  result["students"]["jR"]["grades"]["javascript"] <= 71
) {
  result["students"]["jR"]["grades"]["gp"]["javascript"] = 1;
} else if (
  result["students"]["jR"]["grades"]["javascript"] >= 71 &&
  result["students"]["jR"]["grades"]["javascript"] <= 81
) {
  result["students"]["jR"]["grades"]["gp"]["javascript"] = 2;
} else if (
  result["students"]["jR"]["grades"]["javascript"] >= 81 &&
  result["students"]["jR"]["grades"]["javascript"] <= 91
) {
  result["students"]["jR"]["grades"]["gp"]["javascript"] = 3;
} else if (
  result["students"]["jR"]["grades"]["javascript"] >= 91 &&
  result["students"]["jR"]["grades"]["javascript"] <= 101
) {
  result["students"]["jR"]["grades"]["gp"]["javascript"] = 4;
} else {
  result["students"]["jR"]["grades"]["gp"]["javascript"] = 0;
}

if (
  result["students"]["jR"]["grades"]["react"] >= 51 &&
  result["students"]["jR"]["grades"]["react"] <= 61
) {
  result["students"]["jR"]["grades"]["gp"]["react"] = 0.5;
} else if (
  result["students"]["jR"]["grades"]["react"] >= 61 &&
  result["students"]["jR"]["grades"]["react"] <= 71
) {
  result["students"]["jR"]["grades"]["gp"]["react"] = 1;
} else if (
  result["students"]["jR"]["grades"]["react"] >= 71 &&
  result["students"]["jR"]["grades"]["react"] <= 81
) {
  result["students"]["jR"]["grades"]["gp"]["react"] = 2;
} else if (
  result["students"]["jR"]["grades"]["react"] >= 81 &&
  result["students"]["jR"]["grades"]["react"] <= 91
) {
  result["students"]["jR"]["grades"]["gp"]["react"] = 3;
} else if (
  result["students"]["jR"]["grades"]["react"] >= 91 &&
  result["students"]["jR"]["grades"]["react"] <= 101
) {
  result["students"]["jR"]["grades"]["gp"]["react"] = 4;
} else {
  result["students"]["jR"]["grades"]["gp"]["react"] = 0;
}

if (
  result["students"]["jR"]["grades"]["python"] >= 51 &&
  result["students"]["jR"]["grades"]["python"] <= 61
) {
  result["students"]["jR"]["grades"]["gp"]["python"] = 0.5;
} else if (
  result["students"]["jR"]["grades"]["python"] >= 61 &&
  result["students"]["jR"]["grades"]["python"] <= 71
) {
  result["students"]["jR"]["grades"]["gp"]["python"] = 1;
} else if (
  result["students"]["jR"]["grades"]["python"] >= 71 &&
  result["students"]["jR"]["grades"]["python"] <= 81
) {
  result["students"]["jR"]["grades"]["gp"]["python"] = 2;
} else if (
  result["students"]["jR"]["grades"]["python"] >= 81 &&
  result["students"]["jR"]["grades"]["python"] <= 91
) {
  result["students"]["jR"]["grades"]["gp"]["python"] = 3;
} else if (
  result["students"]["jR"]["grades"]["python"] >= 91 &&
  result["students"]["jR"]["grades"]["python"] <= 101
) {
  result["students"]["jR"]["grades"]["gp"]["python"] = 4;
} else {
  result["students"]["jR"]["grades"]["gp"]["python"] = 0;
}

if (
  result["students"]["jR"]["grades"]["java"] >= 51 &&
  result["students"]["jR"]["grades"]["java"] <= 61
) {
  result["students"]["jR"]["grades"]["gp"]["java"] = 0.5;
} else if (
  result["students"]["jR"]["grades"]["java"] >= 61 &&
  result["students"]["jR"]["grades"]["java"] <= 71
) {
  result["students"]["jR"]["grades"]["gp"]["java"] = 1;
} else if (
  result["students"]["jR"]["grades"]["java"] >= 71 &&
  result["students"]["jR"]["grades"]["java"] <= 81
) {
  result["students"]["jR"]["grades"]["gp"]["java"] = 2;
} else if (
  result["students"]["jR"]["grades"]["java"] >= 81 &&
  result["students"]["jR"]["grades"]["java"] <= 91
) {
  result["students"]["jR"]["grades"]["gp"]["java"] = 3;
} else if (
  result["students"]["jR"]["grades"]["java"] >= 91 &&
  result["students"]["jR"]["grades"]["java"] <= 101
) {
  result["students"]["jR"]["grades"]["gp"]["java"] = 4;
} else {
  result["students"]["jR"]["grades"]["gp"]["java"] = 0;
}
result["students"]["jR"]["grades"]["gpa"] = [];
result["students"]["jR"]["grades"]["gpa"]["javascript"] =
  result["students"]["jR"]["grades"]["gp"]["javascript"] *
  result["subjects"]["credits"]["javascript"];
result["students"]["jR"]["grades"]["gpa"]["react"] =
  result["students"]["jR"]["grades"]["gp"]["react"] *
  result["subjects"]["credits"]["react"];
result["students"]["jR"]["grades"]["gpa"]["python"] =
  result["students"]["jR"]["grades"]["gp"]["python"] *
  result["subjects"]["credits"]["python"];
result["students"]["jR"]["grades"]["gpa"]["java"] =
  result["students"]["jR"]["grades"]["gp"]["java"] *
  result["subjects"]["credits"]["java"];
result["students"]["jR"]["grades"]["gpa"]["average"] =
  (result["students"]["jR"]["grades"]["gpa"]["javascript"] +
    result["students"]["jR"]["grades"]["gpa"]["react"] +
    result["students"]["jR"]["grades"]["gpa"]["python"] +
    result["students"]["jR"]["grades"]["gpa"]["java"]) /
  result["subjects"]["credits"]["summary"];

//kd info
result["students"]["kD"] = [];
result["students"]["kD"]["name"] = "Klod";
result["students"]["kD"]["lastName"] = "Mone";
result["students"]["kD"]["age"] = "19";
//kd grades
result["students"]["kD"]["grades"] = [];
result["students"]["kD"]["grades"]["javascript"] = 77;
result["students"]["kD"]["grades"]["react"] = 52;
result["students"]["kD"]["grades"]["python"] = 92;
result["students"]["kD"]["grades"]["java"] = 67;
result["students"]["kD"]["grades"]["summary"] =
  result["students"]["kD"]["grades"]["javascript"] +
  result["students"]["kD"]["grades"]["react"] +
  result["students"]["kD"]["grades"]["python"] +
  result["students"]["kD"]["grades"]["java"];
result["students"]["kD"]["grades"]["average"] =
  result["students"]["kD"]["grades"]["summary"] / result["subjects"].length;
result["students"]["kD"]["grades"]["gp"] = [];
//calculate gp/gpa
if (
  result["students"]["kD"]["grades"]["javascript"] >= 51 &&
  result["students"]["kD"]["grades"]["javascript"] <= 61
) {
  result["students"]["kD"]["grades"]["gp"]["javascript"] = 0.5;
} else if (
  result["students"]["kD"]["grades"]["javascript"] >= 61 &&
  result["students"]["kD"]["grades"]["javascript"] <= 71
) {
  result["students"]["kD"]["grades"]["gp"]["javascript"] = 1;
} else if (
  result["students"]["kD"]["grades"]["javascript"] >= 71 &&
  result["students"]["kD"]["grades"]["javascript"] <= 81
) {
  result["students"]["kD"]["grades"]["gp"]["javascript"] = 2;
} else if (
  result["students"]["kD"]["grades"]["javascript"] >= 81 &&
  result["students"]["kD"]["grades"]["javascript"] <= 91
) {
  result["students"]["kD"]["grades"]["gp"]["javascript"] = 3;
} else if (
  result["students"]["kD"]["grades"]["javascript"] >= 91 &&
  result["students"]["kD"]["grades"]["javascript"] <= 101
) {
  result["students"]["kD"]["grades"]["gp"]["javascript"] = 4;
} else {
  result["students"]["kD"]["grades"]["gp"]["javascript"] = 0;
}

if (
  result["students"]["kD"]["grades"]["react"] >= 51 &&
  result["students"]["kD"]["grades"]["react"] <= 61
) {
  result["students"]["kD"]["grades"]["gp"]["react"] = 0.5;
} else if (
  result["students"]["kD"]["grades"]["react"] >= 61 &&
  result["students"]["kD"]["grades"]["react"] <= 71
) {
  result["students"]["kD"]["grades"]["gp"]["react"] = 1;
} else if (
  result["students"]["kD"]["grades"]["react"] >= 71 &&
  result["students"]["kD"]["grades"]["react"] <= 81
) {
  result["students"]["kD"]["grades"]["gp"]["react"] = 2;
} else if (
  result["students"]["kD"]["grades"]["react"] >= 81 &&
  result["students"]["kD"]["grades"]["react"] <= 91
) {
  result["students"]["kD"]["grades"]["gp"]["react"] = 3;
} else if (
  result["students"]["kD"]["grades"]["react"] >= 91 &&
  result["students"]["kD"]["grades"]["react"] <= 101
) {
  result["students"]["kD"]["grades"]["gp"]["react"] = 4;
} else {
  result["students"]["kD"]["grades"]["gp"]["react"] = 0;
}

if (
  result["students"]["kD"]["grades"]["python"] >= 51 &&
  result["students"]["kD"]["grades"]["python"] <= 61
) {
  result["students"]["kD"]["grades"]["gp"]["python"] = 0.5;
} else if (
  result["students"]["kD"]["grades"]["python"] >= 61 &&
  result["students"]["kD"]["grades"]["python"] <= 71
) {
  result["students"]["kD"]["grades"]["gp"]["python"] = 1;
} else if (
  result["students"]["kD"]["grades"]["python"] >= 71 &&
  result["students"]["kD"]["grades"]["python"] <= 81
) {
  result["students"]["kD"]["grades"]["gp"]["python"] = 2;
} else if (
  result["students"]["kD"]["grades"]["python"] >= 81 &&
  result["students"]["kD"]["grades"]["python"] <= 91
) {
  result["students"]["kD"]["grades"]["gp"]["python"] = 3;
} else if (
  result["students"]["kD"]["grades"]["python"] >= 91 &&
  result["students"]["kD"]["grades"]["python"] <= 101
) {
  result["students"]["kD"]["grades"]["gp"]["python"] = 4;
} else {
  result["students"]["kD"]["grades"]["gp"]["python"] = 0;
}

if (
  result["students"]["kD"]["grades"]["java"] >= 51 &&
  result["students"]["kD"]["grades"]["java"] <= 61
) {
  result["students"]["kD"]["grades"]["gp"]["java"] = 0.5;
} else if (
  result["students"]["kD"]["grades"]["java"] >= 61 &&
  result["students"]["kD"]["grades"]["java"] <= 71
) {
  result["students"]["kD"]["grades"]["gp"]["java"] = 1;
} else if (
  result["students"]["kD"]["grades"]["java"] >= 71 &&
  result["students"]["kD"]["grades"]["java"] <= 81
) {
  result["students"]["kD"]["grades"]["gp"]["java"] = 2;
} else if (
  result["students"]["kD"]["grades"]["java"] >= 81 &&
  result["students"]["kD"]["grades"]["java"] <= 91
) {
  result["students"]["kD"]["grades"]["gp"]["java"] = 3;
} else if (
  result["students"]["kD"]["grades"]["java"] >= 91 &&
  result["students"]["kD"]["grades"]["java"] <= 101
) {
  result["students"]["kD"]["grades"]["gp"]["java"] = 4;
} else {
  result["students"]["kD"]["grades"]["gp"]["java"] = 0;
}
result["students"]["kD"]["grades"]["gpa"] = [];
result["students"]["kD"]["grades"]["gpa"]["javascript"] =
  result["students"]["kD"]["grades"]["gp"]["javascript"] *
  result["subjects"]["credits"]["javascript"];
result["students"]["kD"]["grades"]["gpa"]["react"] =
  result["students"]["kD"]["grades"]["gp"]["react"] *
  result["subjects"]["credits"]["react"];
result["students"]["kD"]["grades"]["gpa"]["python"] =
  result["students"]["kD"]["grades"]["gp"]["python"] *
  result["subjects"]["credits"]["python"];
result["students"]["kD"]["grades"]["gpa"]["java"] =
  result["students"]["kD"]["grades"]["gp"]["java"] *
  result["subjects"]["credits"]["java"];
result["students"]["kD"]["grades"]["gpa"]["average"] =
  (result["students"]["kD"]["grades"]["gpa"]["javascript"] +
    result["students"]["kD"]["grades"]["gpa"]["react"] +
    result["students"]["kD"]["grades"]["gpa"]["python"] +
    result["students"]["kD"]["grades"]["gpa"]["java"]) /
  result["subjects"]["credits"]["summary"];
//vg info
result["students"]["vG"] = [];
result["students"]["vG"]["name"] = "Van";
result["students"]["vG"]["lastName"] = "Gogh";
result["students"]["vG"]["age"] = "21";
//vg grades
result["students"]["vG"]["grades"] = [];
result["students"]["vG"]["grades"]["javascript"] = 51;
result["students"]["vG"]["grades"]["react"] = 98;
result["students"]["vG"]["grades"]["python"] = 65;
result["students"]["vG"]["grades"]["java"] = 70;
result["students"]["vG"]["grades"]["summary"] =
  result["students"]["vG"]["grades"]["javascript"] +
  result["students"]["vG"]["grades"]["react"] +
  result["students"]["vG"]["grades"]["python"] +
  result["students"]["vG"]["grades"]["java"];
result["students"]["vG"]["grades"]["average"] =
  result["students"]["vG"]["grades"]["summary"] / result["subjects"].length;
result["students"]["vG"]["grades"]["gp"] = [];
//calculate gp/gpa
if (
  result["students"]["vG"]["grades"]["javascript"] >= 51 &&
  result["students"]["vG"]["grades"]["javascript"] <= 61
) {
  result["students"]["vG"]["grades"]["gp"]["javascript"] = 0.5;
} else if (
  result["students"]["vG"]["grades"]["javascript"] >= 61 &&
  result["students"]["vG"]["grades"]["javascript"] <= 71
) {
  result["students"]["vG"]["grades"]["gp"]["javascript"] = 1;
} else if (
  result["students"]["vG"]["grades"]["javascript"] >= 71 &&
  result["students"]["vG"]["grades"]["javascript"] <= 81
) {
  result["students"]["vG"]["grades"]["gp"]["javascript"] = 2;
} else if (
  result["students"]["vG"]["grades"]["javascript"] >= 81 &&
  result["students"]["vG"]["grades"]["javascript"] <= 91
) {
  result["students"]["vG"]["grades"]["gp"]["javascript"] = 3;
} else if (
  result["students"]["vG"]["grades"]["javascript"] >= 91 &&
  result["students"]["vG"]["grades"]["javascript"] <= 101
) {
  result["students"]["vG"]["grades"]["gp"]["javascript"] = 4;
} else {
  result["students"]["vG"]["grades"]["gp"]["javascript"] = 0;
}

if (
  result["students"]["vG"]["grades"]["react"] >= 51 &&
  result["students"]["vG"]["grades"]["react"] <= 61
) {
  result["students"]["vG"]["grades"]["gp"]["react"] = 0.5;
} else if (
  result["students"]["vG"]["grades"]["react"] >= 61 &&
  result["students"]["vG"]["grades"]["react"] <= 71
) {
  result["students"]["vG"]["grades"]["gp"]["react"] = 1;
} else if (
  result["students"]["vG"]["grades"]["react"] >= 71 &&
  result["students"]["vG"]["grades"]["react"] <= 81
) {
  result["students"]["vG"]["grades"]["gp"]["react"] = 2;
} else if (
  result["students"]["vG"]["grades"]["react"] >= 81 &&
  result["students"]["vG"]["grades"]["react"] <= 91
) {
  result["students"]["vG"]["grades"]["gp"]["react"] = 3;
} else if (
  result["students"]["vG"]["grades"]["react"] >= 91 &&
  result["students"]["vG"]["grades"]["react"] <= 101
) {
  result["students"]["vG"]["grades"]["gp"]["react"] = 4;
} else {
  result["students"]["vG"]["grades"]["gp"]["react"] = 0;
}

if (
  result["students"]["vG"]["grades"]["python"] >= 51 &&
  result["students"]["vG"]["grades"]["python"] <= 61
) {
  result["students"]["vG"]["grades"]["gp"]["python"] = 0.5;
} else if (
  result["students"]["vG"]["grades"]["python"] >= 61 &&
  result["students"]["vG"]["grades"]["python"] <= 71
) {
  result["students"]["vG"]["grades"]["gp"]["python"] = 1;
} else if (
  result["students"]["vG"]["grades"]["python"] >= 71 &&
  result["students"]["vG"]["grades"]["python"] <= 81
) {
  result["students"]["vG"]["grades"]["gp"]["python"] = 2;
} else if (
  result["students"]["vG"]["grades"]["python"] >= 81 &&
  result["students"]["vG"]["grades"]["python"] <= 91
) {
  result["students"]["vG"]["grades"]["gp"]["python"] = 3;
} else if (
  result["students"]["vG"]["grades"]["python"] >= 91 &&
  result["students"]["vG"]["grades"]["python"] <= 101
) {
  result["students"]["vG"]["grades"]["gp"]["python"] = 4;
} else {
  result["students"]["vG"]["grades"]["gp"]["python"] = 0;
}

if (
  result["students"]["vG"]["grades"]["java"] >= 51 &&
  result["students"]["vG"]["grades"]["java"] <= 61
) {
  result["students"]["vG"]["grades"]["gp"]["java"] = 0.5;
} else if (
  result["students"]["vG"]["grades"]["java"] >= 61 &&
  result["students"]["vG"]["grades"]["java"] <= 71
) {
  result["students"]["vG"]["grades"]["gp"]["java"] = 1;
} else if (
  result["students"]["vG"]["grades"]["java"] >= 71 &&
  result["students"]["vG"]["grades"]["java"] <= 81
) {
  result["students"]["vG"]["grades"]["gp"]["java"] = 2;
} else if (
  result["students"]["vG"]["grades"]["java"] >= 81 &&
  result["students"]["vG"]["grades"]["java"] <= 91
) {
  result["students"]["vG"]["grades"]["gp"]["java"] = 3;
} else if (
  result["students"]["vG"]["grades"]["java"] >= 91 &&
  result["students"]["vG"]["grades"]["java"] <= 101
) {
  result["students"]["vG"]["grades"]["gp"]["java"] = 4;
} else {
  result["students"]["vG"]["grades"]["gp"]["java"] = 0;
}
result["students"]["vG"]["grades"]["gpa"] = [];
result["students"]["vG"]["grades"]["gpa"]["javascript"] =
  result["students"]["vG"]["grades"]["gp"]["javascript"] *
  result["subjects"]["credits"]["javascript"];
result["students"]["vG"]["grades"]["gpa"]["react"] =
  result["students"]["vG"]["grades"]["gp"]["react"] *
  result["subjects"]["credits"]["react"];
result["students"]["vG"]["grades"]["gpa"]["python"] =
  result["students"]["vG"]["grades"]["gp"]["python"] *
  result["subjects"]["credits"]["python"];
result["students"]["vG"]["grades"]["gpa"]["java"] =
  result["students"]["vG"]["grades"]["gp"]["java"] *
  result["subjects"]["credits"]["java"];
result["students"]["vG"]["grades"]["gpa"]["average"] =
  (result["students"]["vG"]["grades"]["gpa"]["javascript"] +
    result["students"]["vG"]["grades"]["gpa"]["react"] +
    result["students"]["vG"]["grades"]["gpa"]["python"] +
    result["students"]["vG"]["grades"]["gpa"]["java"]) /
  result["subjects"]["credits"]["summary"];
//ds info
result["students"]["dS"] = [];
result["students"]["dS"]["name"] = "Dam";
result["students"]["dS"]["lastName"] = "Square";
result["students"]["dS"]["age"] = "36";
//dS grades
result["students"]["dS"]["grades"] = [];
result["students"]["dS"]["grades"]["javascript"] = 82;
result["students"]["dS"]["grades"]["react"] = 53;
result["students"]["dS"]["grades"]["python"] = 80;
result["students"]["dS"]["grades"]["java"] = 65;
result["students"]["dS"]["grades"]["summary"] =
  result["students"]["dS"]["grades"]["javascript"] +
  result["students"]["dS"]["grades"]["react"] +
  result["students"]["dS"]["grades"]["python"] +
  result["students"]["dS"]["grades"]["java"];
result["students"]["dS"]["grades"]["average"] =
  result["students"]["dS"]["grades"]["summary"] / result["subjects"].length;
result["students"]["dS"]["grades"]["gp"] = [];
//calculate gp/gpa
if (
  result["students"]["dS"]["grades"]["javascript"] >= 51 &&
  result["students"]["dS"]["grades"]["javascript"] <= 61
) {
  result["students"]["dS"]["grades"]["gp"]["javascript"] = 0.5;
} else if (
  result["students"]["dS"]["grades"]["javascript"] >= 61 &&
  result["students"]["dS"]["grades"]["javascript"] <= 71
) {
  result["students"]["dS"]["grades"]["gp"]["javascript"] = 1;
} else if (
  result["students"]["dS"]["grades"]["javascript"] >= 71 &&
  result["students"]["dS"]["grades"]["javascript"] <= 81
) {
  result["students"]["dS"]["grades"]["gp"]["javascript"] = 2;
} else if (
  result["students"]["dS"]["grades"]["javascript"] >= 81 &&
  result["students"]["dS"]["grades"]["javascript"] <= 91
) {
  result["students"]["dS"]["grades"]["gp"]["javascript"] = 3;
} else if (
  result["students"]["dS"]["grades"]["javascript"] >= 91 &&
  result["students"]["dS"]["grades"]["javascript"] <= 101
) {
  result["students"]["dS"]["grades"]["gp"]["javascript"] = 4;
} else {
  result["students"]["dS"]["grades"]["gp"]["javascript"] = 0;
}

if (
  result["students"]["dS"]["grades"]["react"] >= 51 &&
  result["students"]["dS"]["grades"]["react"] <= 61
) {
  result["students"]["dS"]["grades"]["gp"]["react"] = 0.5;
} else if (
  result["students"]["dS"]["grades"]["react"] >= 61 &&
  result["students"]["dS"]["grades"]["react"] <= 71
) {
  result["students"]["dS"]["grades"]["gp"]["react"] = 1;
} else if (
  result["students"]["dS"]["grades"]["react"] >= 71 &&
  result["students"]["dS"]["grades"]["react"] <= 81
) {
  result["students"]["dS"]["grades"]["gp"]["react"] = 2;
} else if (
  result["students"]["dS"]["grades"]["react"] >= 81 &&
  result["students"]["dS"]["grades"]["react"] <= 91
) {
  result["students"]["dS"]["grades"]["gp"]["react"] = 3;
} else if (
  result["students"]["dS"]["grades"]["react"] >= 91 &&
  result["students"]["dS"]["grades"]["react"] <= 101
) {
  result["students"]["dS"]["grades"]["gp"]["react"] = 4;
} else {
  result["students"]["dS"]["grades"]["gp"]["react"] = 0;
}

if (
  result["students"]["dS"]["grades"]["python"] >= 51 &&
  result["students"]["dS"]["grades"]["python"] <= 61
) {
  result["students"]["dS"]["grades"]["gp"]["python"] = 0.5;
} else if (
  result["students"]["dS"]["grades"]["python"] >= 61 &&
  result["students"]["dS"]["grades"]["python"] <= 71
) {
  result["students"]["dS"]["grades"]["gp"]["python"] = 1;
} else if (
  result["students"]["dS"]["grades"]["python"] >= 71 &&
  result["students"]["dS"]["grades"]["python"] <= 81
) {
  result["students"]["dS"]["grades"]["gp"]["python"] = 2;
} else if (
  result["students"]["dS"]["grades"]["python"] >= 81 &&
  result["students"]["dS"]["grades"]["python"] <= 91
) {
  result["students"]["dS"]["grades"]["gp"]["python"] = 3;
} else if (
  result["students"]["dS"]["grades"]["python"] >= 91 &&
  result["students"]["dS"]["grades"]["python"] <= 101
) {
  result["students"]["dS"]["grades"]["gp"]["python"] = 4;
} else {
  result["students"]["dS"]["grades"]["gp"]["python"] = 0;
}

if (
  result["students"]["dS"]["grades"]["java"] >= 51 &&
  result["students"]["dS"]["grades"]["java"] <= 61
) {
  result["students"]["dS"]["grades"]["gp"]["java"] = 0.5;
} else if (
  result["students"]["dS"]["grades"]["java"] >= 61 &&
  result["students"]["dS"]["grades"]["java"] <= 71
) {
  result["students"]["dS"]["grades"]["gp"]["java"] = 1;
} else if (
  result["students"]["dS"]["grades"]["java"] >= 71 &&
  result["students"]["dS"]["grades"]["java"] <= 81
) {
  result["students"]["dS"]["grades"]["gp"]["java"] = 2;
} else if (
  result["students"]["dS"]["grades"]["java"] >= 81 &&
  result["students"]["dS"]["grades"]["java"] <= 91
) {
  result["students"]["dS"]["grades"]["gp"]["java"] = 3;
} else if (
  result["students"]["dS"]["grades"]["java"] >= 91 &&
  result["students"]["dS"]["grades"]["java"] <= 101
) {
  result["students"]["dS"]["grades"]["gp"]["java"] = 4;
} else {
  result["students"]["dS"]["grades"]["gp"]["java"] = 0;
}
result["students"]["dS"]["grades"]["gpa"] = [];
result["students"]["dS"]["grades"]["gpa"]["javascript"] =
  result["students"]["dS"]["grades"]["gp"]["javascript"] *
  result["subjects"]["credits"]["javascript"];
result["students"]["dS"]["grades"]["gpa"]["react"] =
  result["students"]["dS"]["grades"]["gp"]["react"] *
  result["subjects"]["credits"]["react"];
result["students"]["dS"]["grades"]["gpa"]["python"] =
  result["students"]["dS"]["grades"]["gp"]["python"] *
  result["subjects"]["credits"]["python"];
result["students"]["dS"]["grades"]["gpa"]["java"] =
  result["students"]["dS"]["grades"]["gp"]["java"] *
  result["subjects"]["credits"]["java"];
result["students"]["dS"]["grades"]["gpa"]["average"] =
  (result["students"]["dS"]["grades"]["gpa"]["javascript"] +
    result["students"]["dS"]["grades"]["gpa"]["react"] +
    result["students"]["dS"]["grades"]["gpa"]["python"] +
    result["students"]["dS"]["grades"]["gpa"]["java"]) /
  result["subjects"]["credits"]["summary"];

//end of student list

//passing  grade
result["passingGrade"] =
  (result["students"]["jR"]["grades"]["summary"] +
    result["students"]["kD"]["grades"]["summary"] +
    result["students"]["vG"]["grades"]["summary"] +
    result["students"]["dS"]["grades"]["summary"]) /
  (result["subjects"].length * result["students"]["list"].length);

//determine status
result["students"]["jR"]["grades"]["average"] > result["passingGrade"]
  ? (result["students"]["jR"]["status"] = "red diploma")
  : (result["students"]["jR"]["status"] = "public enemy");

result["students"]["kD"]["grades"]["average"] > result["passingGrade"]
  ? (result["students"]["kD"]["status"] = "red diploma")
  : (result["students"]["kD"]["status"] = "public enemy");

result["students"]["vG"]["grades"]["average"] > result["passingGrade"]
  ? (result["students"]["vG"]["status"] = "red diploma")
  : (result["students"]["vG"]["status"] = "public enemy");

result["students"]["dS"]["grades"]["average"] > result["passingGrade"]
  ? (result["students"]["dS"]["status"] = "red diploma")
  : (result["students"]["dS"]["status"] = "public enemy");

//https://youtu.be/GC5E8ie2pdM?t=63
result["students"]["bestStudents"] = [];
//by gpa
if (
  result["students"]["jR"]["grades"]["gpa"]["average"] >
    result["students"]["kD"]["grades"]["gpa"]["average"] &&
  result["students"]["jR"]["grades"]["gpa"]["average"] >
    result["students"]["vG"]["grades"]["gpa"]["average"] &&
  result["students"]["jR"]["grades"]["gpa"]["average"] >
    result["students"]["dS"]["grades"]["gpa"]["average"]
) {
  result["students"]["bestStudents"]["gpa"] = result["students"]["jR"];
} else if (
  result["students"]["kD"]["grades"]["gpa"]["average"] >
    result["students"]["jR"]["grades"]["gpa"]["average"] &&
  result["students"]["kD"]["grades"]["gpa"]["average"] >
    result["students"]["vG"]["grades"]["gpa"]["average"] &&
  result["students"]["kD"]["grades"]["gpa"]["average"] >
    result["students"]["dS"]["grades"]["gpa"]["average"]
) {
  result["students"]["bestStudents"]["gpa"] = result["students"]["kD"];
} else if (
  result["students"]["vG"]["grades"]["gpa"]["average"] >
    result["students"]["jR"]["grades"]["gpa"]["average"] &&
  result["students"]["vG"]["grades"]["gpa"]["average"] >
    result["students"]["kD"]["grades"]["gpa"]["average"] &&
  result["students"]["vG"]["grades"]["gpa"]["average"] >
    result["students"]["dS"]["grades"]["gpa"]["average"]
) {
  result["students"]["bestStudents"]["gpa"] = result["students"]["kD"];
} else {
  result["students"]["bestStudents"] = result["students"]["sD"];
}
//by age
if (
  result["students"]["jR"]["grades"]["gpa"]["average"] &&
  result["students"]["jR"]["age"] >
    result["students"]["kD"]["grades"]["gpa"]["average"] &&
  result["students"]["kD"]["age"] &&
  result["students"]["jR"]["grades"]["gpa"]["average"] &&
  result["students"]["jR"]["age"] >
    result["students"]["vG"]["grades"]["gpa"]["average"] &&
  result["students"]["vG"]["age"] &&
  result["students"]["jR"]["grades"]["gpa"]["average"] &&
  result["students"]["jR"]["age"] >
    result["students"]["dS"]["grades"]["gpa"]["average"] &&
  result["students"]["dS"]["age"]
) {
  result["students"]["bestStudents"]["age"] = result["students"]["jR"];
} else if (
  result["students"]["kD"]["grades"]["gpa"]["average"] &&
  result["students"]["kD"]["age"] >
    result["students"]["jR"]["grades"]["gpa"]["average"] &&
  result["students"]["jR"]["age"] &&
  result["students"]["kD"]["grades"]["gpa"]["average"] &&
  result["students"]["kD"]["age"] >
    result["students"]["vG"]["grades"]["gpa"]["average"] &&
  result["students"]["vG"]["age"] &&
  result["students"]["kD"]["grades"]["gpa"]["average"] &&
  result["students"]["kD"]["age"] >
    result["students"]["dS"]["grades"]["gpa"]["average"] &&
  result["students"]["dS"]["age"]
) {
  result["students"]["bestStudents"]["age"] = result["students"]["kD"];
} else if (
  result["students"]["vG"]["grades"]["gpa"]["average"] &&
  result["students"]["vG"]["age"] >
    result["students"]["jR"]["grades"]["gpa"]["average"] &&
  result["students"]["jR"]["age"] &&
  result["students"]["vG"]["grades"]["gpa"]["average"] &&
  result["students"]["vG"]["age"] >
    result["students"]["kD"]["grades"]["gpa"]["average"] &&
  result["students"]["kD"]["age"] &&
  result["students"]["vG"]["grades"]["gpa"]["average"] &&
  result["students"]["vG"]["age"] >
    result["students"]["dS"]["grades"]["gpa"]["average"] &&
  result["students"]["dS"]["age"]
) {
  result["students"]["bestStudents"]["age"] = result["students"]["vG"];
} else {
  result["students"]["bestStudents"]["age"] = result["students"]["sD"];
}
//by technology
if (
  (result["students"]["jR"]["grades"]["javascript"] +
    result["students"]["jR"]["grades"]["react"]) /
    result["subjects"]["frontend"].length >
    (result["students"]["kD"]["grades"]["javascript"] +
      result["students"]["kD"]["grades"]["react"]) /
      result["subjects"]["frontend"].length &&
  (result["students"]["jR"]["grades"]["javascript"] +
    result["students"]["jR"]["grades"]["react"]) /
    result["subjects"]["frontend"].length >
    (result["students"]["vG"]["grades"]["javascript"] +
      result["students"]["vG"]["grades"]["react"]) /
      result["subjects"]["frontend"].length &&
  (result["students"]["jR"]["grades"]["javascript"] +
    result["students"]["jR"]["grades"]["react"]) /
    result["subjects"]["frontend"].length >
    (result["students"]["dS"]["grades"]["javascript"] +
      result["students"]["dS"]["grades"]["react"]) /
      result["subjects"]["frontend"].length
) {
  result["students"]["bestStudents"]["technology"] = result["students"]["jR"];
} else if (
  (result["students"]["kD"]["grades"]["javascript"] +
    result["students"]["kD"]["grades"]["react"]) /
    result["subjects"]["frontend"].length >
    (result["students"]["jR"]["grades"]["javascript"] +
      result["students"]["jR"]["grades"]["react"]) /
      result["subjects"]["frontend"].length &&
  (result["students"]["kD"]["grades"]["javascript"] +
    result["students"]["kD"]["grades"]["react"]) /
    result["subjects"]["frontend"].length >
    (result["students"]["vG"]["grades"]["javascript"] +
      result["students"]["vG"]["grades"]["react"]) /
      result["subjects"]["frontend"].length &&
  (result["students"]["kD"]["grades"]["javascript"] +
    result["students"]["kD"]["grades"]["react"]) /
    result["subjects"]["frontend"].length >
    (result["students"]["dS"]["grades"]["javascript"] +
      result["students"]["dS"]["grades"]["react"]) /
      result["subjects"]["frontend"].length
) {
  result["students"]["bestStudents"]["technology"] = result["students"]["kD"];
} else if (
  (result["students"]["vG"]["grades"]["javascript"] +
    result["students"]["vG"]["grades"]["react"]) /
    result["subjects"]["frontend"].length >
    (result["students"]["jR"]["grades"]["javascript"] +
      result["students"]["jR"]["grades"]["react"]) /
      result["subjects"]["frontend"].length &&
  (result["students"]["vG"]["grades"]["javascript"] +
    result["students"]["vG"]["grades"]["react"]) /
    result["subjects"]["frontend"].length >
    (result["students"]["kD"]["grades"]["javascript"] +
      result["students"]["kD"]["grades"]["react"]) /
      result["subjects"]["frontend"].length &&
  (result["students"]["vG"]["grades"]["javascript"] +
    result["students"]["vG"]["grades"]["react"]) /
    result["subjects"]["frontend"].length >
    (result["students"]["dS"]["grades"]["javascript"] +
      result["students"]["dS"]["grades"]["react"]) /
      result["subjects"]["frontend"].length
) {
  result["students"]["bestStudents"]["technology"] = result["students"]["vG"];
} else {
  result["students"]["bestStudents"]["technology"] = result["students"]["sD"];
}

console.log(result);
